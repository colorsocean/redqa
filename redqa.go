package redqa

import (
	"encoding/json"
	"fmt"
	"log"
	"time"

	"bitbucket.org/colorsocean/redid"

	"github.com/garyburd/redigo/redis"
)

const (
	keyDialogIDCounter  = "didctr"
	keyMessageIDCounter = "midctr"
)

type Options struct {
	Pool   *redis.Pool
	Prefix string
}

type RedQA struct {
	ops Options
}

func New(ops Options) *RedQA {
	this := &RedQA{
		ops: ops,
	}
	if this.ops.Prefix == "" {
		this.ops.Prefix = "redqa"
	}
	return this
}

func (this *RedQA) Dialog(id int) *DialogModel {
	conn := this.ops.Pool.Get()
	defer conn.Close()

	exists, err := redis.Bool(conn.Do("exists", this._dialog(id)))
	if err != nil {
		panic(err)
	}
	if !exists {
		panic(fmt.Sprintf("Dialog '%d' does not exists", id))
		return nil
	}

	return &DialogModel{
		id: id,
		qa: this,
	}
}

func (this *RedQA) Dialogs(skip, take int) (dialogs []*DialogModel) {
	conn := this.ops.Pool.Get()
	defer conn.Close()

	dialogIds, err := redis.Ints(conn.Do("zrevrange", this._dialogs(), skip, skip+take-1))
	if err != nil {
		panic(err)
	}

	for _, id := range dialogIds {
		dialogs = append(dialogs, this.Dialog(id))
	}

	return
}

func (this *RedQA) DialogsByUser(userID string, skip, take int) (dialogs []*DialogModel) {
	conn := this.ops.Pool.Get()
	defer conn.Close()

	dialogIds, err := redis.Ints(conn.Do("zrevrange", this._dialogsForUser(userID), skip, skip+take-1))
	if err != nil {
		panic(err)
	}

	for _, id := range dialogIds {
		dialogs = append(dialogs, this.Dialog(id))
	}

	return
}

func (this *RedQA) StartDialog(userID, userName, text, category string) (*Dialog, *Message) {
	conn := this.ops.Pool.Get()
	defer conn.Close()

	did := redid.ID{keyDialogIDCounter, conn}

	dialogID, err := did.Obtain(nil)
	if err != nil {
		panic(err)
	}

	dialog := &Dialog{
		ID:       dialogID,
		UserID:   userID,
		UserName: userName,
		Category: category,
		IsOpen:   true,
	}

	dialogData, err := marshal(dialog)
	if err != nil {
		panic(err)
	}

	_, err = conn.Do("set", this._dialog(dialog.ID), dialogData)
	if err != nil {
		panic(err)
	}

	_, err = conn.Do("zadd", this._dialogsForUser(dialog.UserID), time.Now().UTC().UnixNano(), dialog.ID)
	if err != nil {
		panic(err)
	}

	dm := this.Dialog(dialog.ID)

	this.addEvent(conn, Event{
		Dialog: dialog,
	}, dialog.UserID, false)

	message := dm.AddMessage(text, dialog.UserID, dialog.UserName, false)

	return dialog, message
}

func (this *RedQA) addEvent(conn redis.Conn, ev Event, userID string, isAdmin bool) {
	data, err := marshal(ev)
	if err != nil {
		panic(err)
	}

	//	var targetUserID string
	//	if ev.Message != nil {
	//		targetUserID = ev.Message.UserID
	//	}

	if isAdmin {
		_, err = conn.Do("zadd", this._eventsForUser(userID), time.Now().UTC().UnixNano(), data)
		if err != nil {
			panic(err)
		}
		_, err = conn.Do("PUBLISH", this._eventsForUser(userID), "hello")
		if err != nil {
			panic(err)
		}
	} else {
		_, err = conn.Do("zadd", this._eventsForUser("admin"), time.Now().UTC().UnixNano(), data)
		if err != nil {
			panic(err)
		}
		_, err = conn.Do("PUBLISH", this._eventsForUser("admin"), "hello")
		if err != nil {
			panic(err)
		}
	}
}

func (this *RedQA) Subscribe(userID string, isAdmin bool) (func(), chan interface{}) {
	conn := this.ops.Pool.Get()

	if isAdmin {
		userID = "admin"
	}

	recv := make(chan interface{}, 9999)
	yield := make(chan interface{}, 9999)
	exit := make(chan interface{}, 9999)
	exit2 := make(chan interface{}, 9999)

	psc := redis.PubSubConn{conn}
	psc.Subscribe(this._eventsForUser(userID))

	go func() {
		for {
			switch v := psc.Receive().(type) {
			case redis.Message:
				log.Printf("%s: message: %s\n", v.Channel, v.Data)
				recv <- v.Data
			case redis.Subscription:
				log.Printf("%s: %s %d\n", v.Channel, v.Kind, v.Count)
			case error:
				log.Println("Error", v)
				return
			}
		}
	}()

	go func() {
		for {
			select {
			case received := <-recv:
				yield <- received
			case <-time.After(3 * time.Second):
				err := psc.Ping("PING")
				log.Println("Proxy: PING", err)
			case <-exit:
				err := psc.Unsubscribe(this._eventsForUser(userID))
				log.Println("Proxy: UNSUBSCRIBE", err)
				err = psc.Close()
				log.Println("Proxy: CLOSE", err)
				return
			}
		}
	}()

	return func() {
		close(exit)
		close(exit2)
	}, yield
}

func (this *RedQA) SetSynced(userID string, isAdmin bool, ts int64) {
	conn := this.ops.Pool.Get()
	defer conn.Close()

	if isAdmin {
		userID = "admin"
	}

	//> Set Last Sync Stamp
	_, err := conn.Do("zremrangebyscore", this._eventsForUser(userID), "-inf", ts)
	if err != nil {
		panic(err)
	}
}

func (this *RedQA) SyncEvents(userID string, isAdmin bool) (evs []*Event, now int64) {
	conn := this.ops.Pool.Get()
	defer conn.Close()

	now = time.Now().UTC().UnixNano()

	if isAdmin {
		userID = "admin"
	}

	evsData, err := conn.Do("zrangebyscore", this._eventsForUser(userID), "-inf", now)
	if err != nil {
		panic(err)
	}

	for _, evDataVal := range evsData.([]interface{}) {
		evData, _ := redis.Bytes(evDataVal, nil)

		ev := new(Event)

		err = unmarshal(evData, &ev)
		if err != nil {
			panic(err)
		}

		evs = append(evs, ev)
	}

	return
}

func (this *RedQA) MarkRead(message Message) {
	if message.ID == 0 {
		return
	}
	if message.Read {
		return
	}

	conn := this.ops.Pool.Get()
	defer conn.Close()

	message.Read = true

	messageData, err := marshal(message)
	if err != nil {
		panic(err)
	}

	//> Write message
	_, err = conn.Do("set", this._message(message.ID), messageData)
	if err != nil {
		panic(err)
	}

	log.Println("MSG MARKED:", message.ID, message.ByAdmin, message.Read)

	return
}

func (this *RedQA) _dialog(dialogID int) string {
	return fmt.Sprint(this.ops.Prefix, ":d:", dialogID)
}
func (this *RedQA) _dialogs() string {
	return fmt.Sprint(this.ops.Prefix, ":ds") // ZSET
}

func (this *RedQA) _dialogsForUser(userID string) string {
	return fmt.Sprint(this.ops.Prefix, ":d+u:", userID) // ZSET
}

func (this *RedQA) _message(messageID int) string {
	return fmt.Sprint(this.ops.Prefix, ":m:", messageID)
}

func (this *RedQA) _messagesForDialog(dialogID int) string {
	return fmt.Sprint(this.ops.Prefix, ":m+d:", dialogID) // ZSET
}

func (this *RedQA) _eventsForUser(userID string) string {
	return fmt.Sprint(this.ops.Prefix, ":e+u", userID) // ZSET
}

func marshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

func unmarshal(data []byte, vOut interface{}) error {
	return json.Unmarshal(data, vOut)
}
