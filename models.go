package redqa

import (
	"time"

	"github.com/garyburd/redigo/redis"

	"bitbucket.org/colorsocean/redid"
)

type Status string

var (
	StatusNew    Status = "new"
	StatusOpen   Status = "open"
	StatusClosed Status = "closed"
)

type Dialog struct {
	ID       int
	UserID   string
	UserName string
	Category string
	IsOpen   bool
}

type Message struct {
	ID         int
	DialogID   int
	Text       string
	Time       int64
	ByAdmin    bool
	UserID     string
	SenderName string

	Read bool

	DialogClosed bool
}

type Event struct {
	Message      *Message `json:",omitempty"`
	Dialog       *Dialog  `json:",omitempty"`
	UserOnline   string   `json:",omitempty"`
	UserOffline  string   `json:",omitempty"`
	AdminOnline  string   `json:",omitempty"`
	AdminOffline string   `json:",omitempty"`
}

type DialogModel struct {
	id int
	qa *RedQA
}

func (this *DialogModel) dialogUpdated(userID string, conn redis.Conn) {
	//> Raise dialog in User index up by updating it's score
	_, err := conn.Do("zadd", this.qa._dialogsForUser(userID), time.Now().UTC().UnixNano(), this.id)
	if err != nil {
		panic(err)
	}

	//> Raise dialog in Dialogs index up by updating it's score
	_, err = conn.Do("zadd", this.qa._dialogs(), time.Now().UTC().UnixNano(), this.id)
	if err != nil {
		panic(err)
	}
}

func (this *DialogModel) Info() (dialog *Dialog) {
	conn := this.qa.ops.Pool.Get()
	defer conn.Close()

	data, err := redis.Bytes(conn.Do("get", this.qa._dialog(this.id)))
	if err != nil {
		panic(err)
	}

	err = unmarshal(data, &dialog)
	if err != nil {
		panic(err)
	}
	return
}

func (this *DialogModel) Messages(skip, take int) (msgs []*Message) {
	conn := this.qa.ops.Pool.Get()
	defer conn.Close()

	msgIds, err := redis.Ints(conn.Do("zrevrange", this.qa._messagesForDialog(this.id), skip, skip+take-1))
	if err != nil {
		panic(err)
	}

	for _, id := range msgIds {
		data, err := redis.Bytes(conn.Do("get", this.qa._message(id)))
		if err != nil {
			panic(err)
		}

		msg := new(Message)

		err = unmarshal(data, &msg)
		if err != nil {
			panic(err)
		}

		msgs = append(msgs, msg)
	}

	return
}

func (this *DialogModel) AddMessage(text, userID, userName string, byAdmin bool) *Message {
	conn := this.qa.ops.Pool.Get()
	defer conn.Close()

	if !this.Info().IsOpen {
		panic("Adding message to closed dialog")
	}

	now := time.Now().UTC().UnixNano()

	midgen := redid.ID{keyMessageIDCounter, conn}

	messageID, err := midgen.Obtain(nil)
	if err != nil {
		panic(err)
	}

	message := &Message{
		ID:         messageID,
		DialogID:   this.id,
		Text:       text,
		Time:       time.Now().UTC().UnixNano(),
		ByAdmin:    byAdmin,
		UserID:     userID,
		SenderName: userName,
	}

	messageData, err := marshal(message)
	if err != nil {
		panic(err)
	}

	//> Write message
	_, err = conn.Do("set", this.qa._message(message.ID), messageData)
	if err != nil {
		panic(err)
	}

	//> Attach to dialog
	_, err = conn.Do("zadd", this.qa._messagesForDialog(this.id), now, message.ID)
	if err != nil {
		panic(err)
	}

	this.dialogUpdated(message.UserID, conn)

	this.qa.addEvent(conn, Event{
		Message: message,
	}, userID, byAdmin)

	return message
}

func (this *DialogModel) Close() {
	conn := this.qa.ops.Pool.Get()
	defer conn.Close()

	dialog := this.Info()

	if !dialog.IsOpen {
		panic("Attempting to close already closed dialog")
	}

	dialog.IsOpen = false

	data, err := marshal(dialog)
	if err != nil {
		panic(err)
	}

	_, err = conn.Do("set", this.qa._dialog(dialog.ID), data)
	if err != nil {
		panic(err)
	}

	return
}
